// Utility to create graphs from a list of edges and extract the sub-graphs

package node

import (
)

type Node struct {
    Name        string
    Neighbours  map[string]*Node
    Visited     bool
}

func (n *Node) HasNeighbour(nb *Node) bool {
    if _, ok := n.Neighbours[nb.Name]; !ok {
        return false
    }
    return true
}

func (n *Node) IsEqual(nb *Node) bool {
    return n.Name == nb.Name
}

func (n *Node) AddNeighbour(to *Node){
    n.Neighbours[to.Name] = to
}

func NewNode(n string) *Node {
    node := new(Node)
    node.Name = n
    node.Neighbours = make(map[string]*Node)
    return node
}

func (n *Node) explore() Stack {
    s := make(Stack, 0)
    for _, nb := range n.Neighbours {
        if !nb.Visited {
            nb.Visited = true
            s = s.Push(nb)
        }
    }
//   fmt.Printf("explore : Node %v has %v unvisited neighbours\n\t[", n.Name, len(s))
//   for _, node := range s {
//      fmt.Printf(" %v ", node.Name)
//   }
//   fmt.Printf("]\n")
    return s
}

func (n *Node) Explore() []*Node {
    visited := make([]*Node, 0)
    var current *Node
    var neighbours Stack
    // Create and initialize stack for ecplore loop
    s := make(Stack, 0)
//    fmt.Printf("Explore : Starting, Stack has %v unvisited nodes\n", len(s))
    n.Visited = true
    s = s.Push(n)
//   fmt.Printf("Explore : Stack has %v unvisited nodes\n\t[", len(s))
//   for _, node := range s {
//       fmt.Printf(" %v ", node.Name)
//   }
//   fmt.Printf("]\n")
    // As long as we find unxplored nodes, carry on exploring
    for !s.IsEmpty() {
//        fmt.Println(len(s))
        current, s = s.Pop()
//        fmt.Println(current.Name, len(s))
        // Mark as visited to only explore once
        current.Visited = true
        visited = append(visited, current)
        // Find all neighbours that haven't already been explored
        neighbours = current.explore()
//        fmt.Printf("explore : found %v neighbours to node %v\n", len(neighbours), current.Name)
        // Push to stack for futur exploration
        s = s.Push(neighbours...)

//       fmt.Printf("Explore loop : Stack has %v unvisited nodes\n\t[", len(s))
//       for _, node := range s {
//           fmt.Printf(" %v ", node.Name)
//       }
//       fmt.Printf("]\n")
    }
    return visited
}

type Stack []*Node

func (s Stack) Pop() (*Node, Stack) {
    return s[len(s)-1], s[:len(s)-1]
}

func (s Stack) Push(n ...*Node) (Stack) {
    return append(s, n...)
}

func (s Stack) IsEmpty() bool {
    return len(s) <= 0
}
